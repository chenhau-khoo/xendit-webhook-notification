package co.xendit.webhookdb.repo;

import co.xendit.webhookdb.entity.WebhookSubscriber;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WebhookSubscriberRepo extends JpaRepository<WebhookSubscriber, String> {
}
