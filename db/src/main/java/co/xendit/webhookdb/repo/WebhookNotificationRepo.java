package co.xendit.webhookdb.repo;

import co.xendit.webhookdb.entity.WebhookNotification;
import co.xendit.webhookdb.enums.TransactionStatus;
import co.xendit.webhookdb.enums.WebhookNotificationStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface WebhookNotificationRepo extends JpaRepository<WebhookNotification, String> {
    Optional<WebhookNotification> findByWebhookSubscriberIdAndTransactionIdAndTransactionStatus(String webhookSubscriberId, String transactionId, TransactionStatus transactionStatus);

    List<WebhookNotification> findByStatusAndNoOfRetryLessThan(WebhookNotificationStatus status, Integer noOfRetry);
}
