package co.xendit.webhookdb.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "webhook_subscriber")
public class WebhookSubscriber extends BaseEntity {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    private String id;

    private String clientId;

    private String url;

    private String secret;

    private Boolean isActive;

    @OneToMany(mappedBy = "webhookSubscriber", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<WebhookNotification> webhookNotifications = new ArrayList<>();
}
