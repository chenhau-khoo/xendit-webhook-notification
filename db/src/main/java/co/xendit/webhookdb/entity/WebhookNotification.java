package co.xendit.webhookdb.entity;

import co.xendit.webhookdb.enums.TransactionStatus;
import co.xendit.webhookdb.enums.WebhookNotificationStatus;
import lombok.Data;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Data
@Entity
@Table(name = "webhook_notification",
        indexes = {@Index(columnList = "status")},
        uniqueConstraints = {@UniqueConstraint(columnNames = {"webhook_subscriber_id", "transaction_id", "transaction_status"})})
public class WebhookNotification extends BaseEntity {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    private String id;

    @Column(nullable = false, name = "transaction_id")
    private String transactionId;

    @Column(nullable = false, name = "transaction_status")
    @Enumerated(EnumType.STRING)
    private TransactionStatus transactionStatus;

    private Integer noOfRetry = 0;

    @Column(nullable = false, name = "status")
    @Enumerated(EnumType.STRING)
    private WebhookNotificationStatus status;

    private String failureReason;

    @OneToOne(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name = "webhook_subscriber_id", referencedColumnName = "id", nullable = false)
    private WebhookSubscriber webhookSubscriber;
}
