package co.xendit.webhookdb.enums;

public enum TransactionStatus {
    PENDING,
    SUCCESS,
    FAILED,
    REFUNDED
}
