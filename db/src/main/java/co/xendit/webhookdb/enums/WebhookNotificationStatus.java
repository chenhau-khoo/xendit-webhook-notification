package co.xendit.webhookdb.enums;

public enum WebhookNotificationStatus {
    PENDING,
    SUCCESS,
    ERROR
}
