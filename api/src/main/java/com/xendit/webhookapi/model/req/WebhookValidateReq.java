package com.xendit.webhookapi.model.req;

import co.xendit.webhookdb.enums.TransactionStatus;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class WebhookValidateReq {

    @NotBlank
    private String webhookId;

    @NotBlank
    private String transactionId;

    @NotNull
    private TransactionStatus transactionStatus;

    @NotNull
    private BigDecimal amount;

    @NotNull
    private String description;

}
