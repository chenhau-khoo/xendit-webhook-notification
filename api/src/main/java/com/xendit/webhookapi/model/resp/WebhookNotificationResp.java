package com.xendit.webhookapi.model.resp;

import co.xendit.webhookdb.enums.WebhookNotificationStatus;
import lombok.Data;

@Data
public class WebhookNotificationResp {
    private String id;
    private WebhookNotificationStatus status;
}
