package com.xendit.webhookapi.model;

import co.xendit.webhookdb.enums.TransactionStatus;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class Notification {
    private String id;
    private String transactionId;
    private TransactionStatus transactionStatus;
    private BigDecimal amount;
    private String description;
    private String shv;
}
