package com.xendit.webhookapi.model.req;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class WebhookSubscriberReq {
    @NotBlank
    private String clientId;
    @NotBlank
    private String url;
}
