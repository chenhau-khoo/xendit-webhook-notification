package com.xendit.webhookapi.model.req;

import lombok.Data;

@Data
public class WebhookSubscriberUpdateReq {
    private String url;
    private Boolean isActive;
}
