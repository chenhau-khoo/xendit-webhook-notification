package com.xendit.webhookapi.model.resp;

import lombok.Data;

@Data
public class WebhookValidateResp {
    private Boolean isValid;
}
