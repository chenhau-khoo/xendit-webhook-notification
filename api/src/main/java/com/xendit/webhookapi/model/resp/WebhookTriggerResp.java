package com.xendit.webhookapi.model.resp;

import lombok.Data;

@Data
public class WebhookTriggerResp {
    private String notificationId;
}
