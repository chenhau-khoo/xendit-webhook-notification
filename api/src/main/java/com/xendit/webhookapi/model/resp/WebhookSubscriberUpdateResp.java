package com.xendit.webhookapi.model.resp;

import lombok.Data;

@Data
public class WebhookSubscriberUpdateResp {
    private String id;
    private String clientId;
    private String url;
    private Boolean isActive;
}
