package com.xendit.webhookapi.model;

import lombok.Data;

@Data
public class WebhookNotificationResult {
    private String notificationId;
    private Boolean isSuccess;
    private String errorMessage;
}
