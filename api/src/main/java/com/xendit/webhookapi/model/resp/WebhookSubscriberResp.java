package com.xendit.webhookapi.model.resp;

import lombok.Data;

@Data
public class WebhookSubscriberResp {
    private String id;
    private String secretKey;
    private Boolean isActive;
    private String clientId;
}
