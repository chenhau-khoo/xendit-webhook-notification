package com.xendit.webhookapi.controller;

import co.xendit.webhookdb.entity.WebhookNotification;
import com.xendit.webhookapi.model.Notification;
import com.xendit.webhookapi.model.req.WebhookTriggerReq;
import com.xendit.webhookapi.model.resp.WebhookNotificationResp;
import com.xendit.webhookapi.model.resp.WebhookTriggerResp;
import com.xendit.webhookapi.model.resp.WebhookValidateResp;
import com.xendit.webhookapi.service.WebhookService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@RestController
@RequestMapping("/v1/webhook")
public class WebhookController {

    @Autowired
    private WebhookService webhookService;

    @ApiOperation(value = "Trigger a webhook notification")
    @PostMapping(value = "/trigger")
    public WebhookTriggerResp trigger(@Valid @RequestBody WebhookTriggerReq webhookTriggerReq) throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException {
        return webhookService.trigger(webhookTriggerReq);
    }

    @ApiOperation(value = "This endpoint is created to validate a webhook notification")
    @PostMapping(value = "/validate/{id}")
    public WebhookValidateResp validate(@PathVariable("id") String webhookSubscriberId, @Valid @RequestBody Notification notification) throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException {
        return webhookService.validate(webhookSubscriberId, notification);
    }

    @ApiOperation(value = "Get a webhook notification status")
    @GetMapping(value = "/notification/{id}")
    public WebhookNotificationResp get(@PathVariable("id") String notificationId) {
        return webhookService.get(notificationId);
    }

}
