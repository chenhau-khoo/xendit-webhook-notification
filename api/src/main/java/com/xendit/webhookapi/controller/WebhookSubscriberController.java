package com.xendit.webhookapi.controller;

import com.xendit.webhookapi.model.req.WebhookSubscriberReq;
import com.xendit.webhookapi.model.req.WebhookSubscriberUpdateReq;
import com.xendit.webhookapi.model.resp.WebhookSubscriberResp;
import com.xendit.webhookapi.model.resp.WebhookSubscriberUpdateResp;
import com.xendit.webhookapi.service.WebhookSubscriberService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/v1/webhook/subscriber")
public class WebhookSubscriberController {

    @Autowired
    private WebhookSubscriberService webhookSubscriberService;

    @ApiOperation(value = "Subscribe a new webhook URL", notes = "A secret key will be generated after successfully registered a new webhook")
    @PostMapping
    public WebhookSubscriberResp subscribe(@Valid @RequestBody WebhookSubscriberReq webhookSubscriberReq) {
        return webhookSubscriberService.subscribe(webhookSubscriberReq);
    }

    @ApiOperation(value = "Update subscriber webhook by Id")
    @PutMapping(value = "/{id}")
    public WebhookSubscriberUpdateResp update(@PathVariable("id") String id, @Valid @RequestBody WebhookSubscriberUpdateReq webhookSubscriberUpdateReq) {
        return webhookSubscriberService.update(id, webhookSubscriberUpdateReq);
    }
}
