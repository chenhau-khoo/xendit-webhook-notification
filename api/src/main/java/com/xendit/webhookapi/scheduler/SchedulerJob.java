package com.xendit.webhookapi.scheduler;

import co.xendit.webhookdb.entity.WebhookNotification;
import co.xendit.webhookdb.enums.WebhookNotificationStatus;
import co.xendit.webhookdb.repo.WebhookNotificationRepo;
import com.xendit.webhookapi.model.Notification;
import com.xendit.webhookapi.service.ClientWebhookApiService;
import com.xendit.webhookapi.utils.HashUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@Slf4j
@Component
public class SchedulerJob {

    @Autowired
    private WebhookNotificationRepo webhookNotificationRepo;

    @Autowired
    private ClientWebhookApiService clientWebhookApiService;

    @Scheduled(fixedRate = 10000)
    public void recoverFailureNotification() throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException {
        List<WebhookNotification> webhookNotifications = webhookNotificationRepo.findByStatusAndNoOfRetryLessThan(WebhookNotificationStatus.ERROR, 3);
        if (webhookNotifications != null) {
            for (WebhookNotification webhookNotification : webhookNotifications) {
                clientWebhookApiService.apiCall(webhookNotification.getWebhookSubscriber().getUrl(), constructNotification(webhookNotification))
                        .whenComplete((result, t) -> {
                            if (t == null) {
                                WebhookNotification notification = webhookNotificationRepo.findById(result.getNotificationId())
                                        .orElseThrow(() -> new IllegalArgumentException("Invalid webhook notification id: " + result.getNotificationId()));
                                notification.setNoOfRetry(notification.getNoOfRetry() + 1);
                                if (result.getIsSuccess()) {
                                    notification.setStatus(WebhookNotificationStatus.SUCCESS);
                                    notification.setFailureReason(null);
                                } else {
                                    notification.setStatus(WebhookNotificationStatus.ERROR);
                                    notification.setFailureReason(result.getErrorMessage());
                                }
                                log.info("notificationId: " + notification.getId() + " noOfRetry: " + notification.getNoOfRetry());
                                webhookNotificationRepo.save(notification);
                            }
                        });
            }
        }
    }

    private Notification constructNotification(WebhookNotification webhookNotification) throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException {
        Notification notification = new Notification();
        notification.setId(webhookNotification.getId());
        // mock
        notification.setAmount(new BigDecimal(50));
        // mock
        notification.setDescription("This is Description");
        notification.setTransactionId(webhookNotification.getTransactionId());
        notification.setTransactionStatus(webhookNotification.getTransactionStatus());
        String hashInput = notification.getTransactionId().concat("|")
                .concat(notification.getTransactionStatus().name()).concat("|")
                .concat(webhookNotification.getWebhookSubscriber().getSecret());
        String hashValue = HashUtil.hmacDigest(hashInput, webhookNotification.getWebhookSubscriber().getSecret(), "HmacSHA256").toUpperCase();
        notification.setShv(hashValue);
        return notification;
    }

}
