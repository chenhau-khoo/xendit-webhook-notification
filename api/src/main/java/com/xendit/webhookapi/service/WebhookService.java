package com.xendit.webhookapi.service;

import co.xendit.webhookdb.entity.WebhookNotification;
import co.xendit.webhookdb.entity.WebhookSubscriber;
import co.xendit.webhookdb.enums.WebhookNotificationStatus;
import co.xendit.webhookdb.repo.WebhookNotificationRepo;
import co.xendit.webhookdb.repo.WebhookSubscriberRepo;
import com.xendit.webhookapi.model.Notification;
import com.xendit.webhookapi.model.req.WebhookTriggerReq;
import com.xendit.webhookapi.model.resp.WebhookNotificationResp;
import com.xendit.webhookapi.model.resp.WebhookTriggerResp;
import com.xendit.webhookapi.model.resp.WebhookValidateResp;
import com.xendit.webhookapi.utils.HashUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@Service
public class WebhookService {

    @Autowired
    private WebhookNotificationRepo webhookNotificationRepo;

    @Autowired
    private WebhookSubscriberRepo webhookSubscriberRepo;

    @Autowired
    private ClientWebhookApiService clientWebhookApiService;

    public WebhookTriggerResp trigger(WebhookTriggerReq req) throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException {
        WebhookTriggerResp resp = new WebhookTriggerResp();
        WebhookSubscriber subscriber = webhookSubscriberRepo.findById(req.getWebhookSubscriberId())
                .orElseThrow(() -> new IllegalArgumentException("Invalid webhookId: " + req.getWebhookSubscriberId()));
        WebhookNotification webhookNotification = webhookNotificationRepo.findByWebhookSubscriberIdAndTransactionIdAndTransactionStatus(req.getWebhookSubscriberId(), req.getTransactionId(), req.getTransactionStatus())
                .orElse(null);
        if (webhookNotification != null) {
            // do nothing as the notification already triggered previously
            resp.setNotificationId(webhookNotification.getId());
            return resp;
        }
        webhookNotification = new WebhookNotification();
        webhookNotification.setTransactionId(req.getTransactionId());
        webhookNotification.setTransactionStatus(req.getTransactionStatus());
        webhookNotification.setWebhookSubscriber(subscriber);
        webhookNotification.setStatus(WebhookNotificationStatus.PENDING);
        webhookNotificationRepo.save(webhookNotification);
        Notification notification = createNotification(req, webhookNotification, subscriber.getSecret());
        sendNotification(subscriber.getUrl(), notification);
        resp.setNotificationId(webhookNotification.getId());
        return resp;
    }

    public WebhookValidateResp validate(String webhookSubscriberId, Notification notification) throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException {
        WebhookSubscriber subscriber = webhookSubscriberRepo.findById(webhookSubscriberId)
                .orElseThrow(() -> new IllegalArgumentException("Invalid webhookId: " + webhookSubscriberId));
        String hashInput = notification.getId().concat("|")
                .concat(notification.getTransactionId()).concat("|")
                .concat(notification.getTransactionStatus().name()).concat("|")
                .concat(subscriber.getSecret());
        String hashValue = HashUtil.hmacDigest(hashInput, subscriber.getSecret(), "HmacSHA256").toUpperCase();
        if (!hashValue.equals(notification.getShv())) {
            throw new IllegalArgumentException("Invalid webhook message");
        }
        WebhookValidateResp resp = new WebhookValidateResp();
        resp.setIsValid(true);
        return resp;
    }

    public WebhookNotificationResp get(String notificationId) {
        WebhookNotification webhookNotification = webhookNotificationRepo.findById(notificationId)
                .orElseThrow(() -> new IllegalArgumentException("Invalid notificationId: " + notificationId));
        WebhookNotificationResp resp = new WebhookNotificationResp();
        resp.setId(webhookNotification.getId());
        resp.setStatus(webhookNotification.getStatus());
        return resp;
    }

    private Notification createNotification(WebhookTriggerReq req, WebhookNotification webhookNotification, String secretKey) throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException {
        Notification notification = new Notification();
        notification.setId(webhookNotification.getId());
        notification.setTransactionId(webhookNotification.getTransactionId());
        notification.setTransactionStatus(webhookNotification.getTransactionStatus());
        notification.setAmount(req.getAmount());
        notification.setDescription(req.getDescription());
        String hashInput = notification.getId().concat("|")
                .concat(notification.getTransactionId()).concat("|")
                .concat(notification.getTransactionStatus().name()).concat("|")
                .concat(secretKey);
        String hashValue = HashUtil.hmacDigest(hashInput, secretKey, "HmacSHA256").toUpperCase();
        notification.setShv(hashValue);
        return notification;
    }

    private void sendNotification(String url, Notification notification) {
        clientWebhookApiService.apiCall(url, notification)
                .whenComplete((result, t) -> {
                    if (t == null) {
                        WebhookNotification webhookNotification = webhookNotificationRepo.findById(result.getNotificationId())
                                .orElseThrow(() -> new IllegalArgumentException("Invalid webhook notification id: " + result.getNotificationId()));
                        if (result.getIsSuccess()) {
                            webhookNotification.setStatus(WebhookNotificationStatus.SUCCESS);
                        } else {
                            webhookNotification.setStatus(WebhookNotificationStatus.ERROR);
                            webhookNotification.setFailureReason(result.getErrorMessage());
                        }
                        webhookNotificationRepo.save(webhookNotification);
                    }
                });
    }
}
