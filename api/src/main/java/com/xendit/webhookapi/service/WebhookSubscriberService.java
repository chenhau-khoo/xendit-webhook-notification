package com.xendit.webhookapi.service;

import co.xendit.webhookdb.entity.WebhookSubscriber;
import co.xendit.webhookdb.repo.WebhookSubscriberRepo;
import com.xendit.webhookapi.model.req.WebhookSubscriberReq;
import com.xendit.webhookapi.model.req.WebhookSubscriberUpdateReq;
import com.xendit.webhookapi.model.resp.WebhookSubscriberResp;
import com.xendit.webhookapi.model.resp.WebhookSubscriberUpdateResp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.UUID;

@Service
public class WebhookSubscriberService {

    @Autowired
    private WebhookSubscriberRepo webhookSubscriberRepo;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public WebhookSubscriberResp subscribe(WebhookSubscriberReq req) {
        WebhookSubscriber webhookSubscriber = new WebhookSubscriber();
        webhookSubscriber.setIsActive(Boolean.FALSE);
        webhookSubscriber.setClientId(req.getClientId());
        webhookSubscriber.setUrl(req.getUrl());
        webhookSubscriber.setClientId(webhookSubscriber.getClientId());
        String secretKey = UUID.randomUUID().toString();
        webhookSubscriber.setSecret(passwordEncoder.encode(secretKey));
        webhookSubscriberRepo.save(webhookSubscriber);
        WebhookSubscriberResp resp = new WebhookSubscriberResp();
        resp.setId(webhookSubscriber.getId());
        resp.setSecretKey(secretKey);
        resp.setIsActive(webhookSubscriber.getIsActive());
        resp.setClientId(webhookSubscriber.getClientId());
        return resp;
    }

    public WebhookSubscriberUpdateResp update(String id, WebhookSubscriberUpdateReq req) {
        WebhookSubscriber webhookSubscriber = webhookSubscriberRepo.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid webhookId: " + id));
        if (!StringUtils.isEmpty(req.getUrl())) {
            webhookSubscriber.setUrl(req.getUrl());
        }
        if (req.getIsActive() != null) {
            webhookSubscriber.setIsActive(req.getIsActive());
        }
        webhookSubscriberRepo.save(webhookSubscriber);
        WebhookSubscriberUpdateResp resp = new WebhookSubscriberUpdateResp();
        resp.setIsActive(webhookSubscriber.getIsActive());
        resp.setId(webhookSubscriber.getId());
        resp.setUrl(webhookSubscriber.getUrl());
        resp.setClientId(webhookSubscriber.getClientId());
        return resp;
    }

}
