package com.xendit.webhookapi.service;

import com.xendit.webhookapi.model.Notification;
import com.xendit.webhookapi.model.WebhookNotificationResult;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.CompletableFuture;

@Service
public class ClientWebhookApiService {

    @Autowired
    private RestTemplate restTemplate;

    @Async
    public CompletableFuture<WebhookNotificationResult> apiCall(String url, Notification notification) {
        HttpEntity<Notification> requestBody = new HttpEntity<>(notification);
        WebhookNotificationResult result = new WebhookNotificationResult();
        result.setNotificationId(notification.getId());
        try {
            restTemplate.exchange(url, HttpMethod.POST, requestBody, String.class);
        } catch (Exception ex) {
            result.setIsSuccess(Boolean.FALSE);
            result.setErrorMessage(ExceptionUtils.getMessage(ex));
            return CompletableFuture.completedFuture(result);
        }
        result.setIsSuccess(Boolean.TRUE);
        return CompletableFuture.completedFuture(result);
    }

}
