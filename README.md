# Xendit Webhook Notification

## Introduction

Webhook Notification service allows the webhook to be triggered to client to inform about a transaction status.

## Prerequisite

- Setup MySQL
    ```shell script
    mysql -u root --password
    create database xendit_webhook;
    ``` 
## Maven
 
 It is advised to use Maven to achieve fast build time during
 development.
 
### Run Build (Include All Tests)

```
mvn clean install
```

## Start localhost

```
cd api/
mvn spring-boot:run -Dspring-boot.run.profiles=local
```

## Postman Collection

 Import postman collection to start playing with the endpoints
 
https://www.getpostman.com/collections/e23abd8d25353b249150

## API Swagger Doc

API Doc can be found at http://localhost:8080/swagger-ui.html

## How do I validate a webhook message?

- A field called `shv` is send along with the webhook notification. 
- The `shv` field is encrpyted with `HmacSHA256` method.
- The combination of the encryption is ```notificationId + "|" + transactionId + "|" + transactionStatus + "|" + webhookSecretKey```